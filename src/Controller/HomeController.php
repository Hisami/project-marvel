<?php

namespace App\Controller;

use App\Service\CallApiService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class HomeController extends AbstractController
{   
    
    #[Route('/home', name: 'app_home')]
    public function index(CallApiService $callApiService): Response
    {   

        $results=$callApiService->getCommics();
        $comics=$results["data"]["results"];

        return $this->render('home/index.html.twig', [
            'comics' => $comics
        ]);
    }

    #[Route('/characters/{comicId}', name: 'characters')]
    public function characterPage(CallApiService $callApiService,int $comicId): Response
    {   
        $results=$callApiService->getCharactersbyCommic($comicId);
        $characters=$results["data"]["results"];

        return $this->render('Characters/index.html.twig', [
            'characters'=>$characters
        ]);
    }
}
