<?php

namespace App\Service;

use Symfony\Contracts\HttpClient\HttpClientInterface;

class CallApiService
{   
    const BASE_URL = 'https://gateway.marvel.com/v1/public/comics';
    const PUBLIC_KEY ='505bd92dcb12b49e279dd83e880329b1';
    //HASH= md5(ts + privKey + pubKey),pour md5 hash:http://www.md5.cz/
    const HASH ='93a074af0c7eedaa1694fbcd29ee83db';
    const TIME_STAMP =1234567890;
    private HttpClientInterface $httpClient;
    private string $url;

    private string $pass='&ts='. self::TIME_STAMP . '&apikey=' . self::PUBLIC_KEY  . '&hash=' . self::HASH;

    public function __construct(HttpClientInterface $httpClient)
    {
        $this->httpClient = $httpClient;
    }

    /**
     * Retourne les données des comic de Avengers
     */
    public function getCommics($limit = 50): array
    { 
        $this->url = self::BASE_URL . '?titleStartsWith=Avengers&orderBy=onsaleDate'.$this->pass;
        $response = $this->httpClient->request('GET', $this->url. '&limit=' . $limit );
        return $response->toArray();
    }

    /**
     * Retourne les données des characters de Avengers à partir de idéntifiant de comic
     */

    public function getCharactersbyCommic(int $id): array
    {
        $this->url = self::BASE_URL .'/'.$id.'/characters?orderBy=-name'.$this->pass;
        $response = $this->httpClient->request('GET', $this->url);
        return $response->toArray();
    }


}

