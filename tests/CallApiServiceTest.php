<?php

namespace App\Tests;

use App\Service\CallApiService;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
//pour tester CallApiService
class CallApiServiceTest extends KernelTestCase
{
    private CallApiService $callApiService;

    protected function setUp(): void
    {
        self::bootKernel();
        $this->callApiService= static::getContainer()->get(CallApiService::class);
    }
    //pour test de getCommics(Avengers/50)de CallApiService
    public function testGetCommicsOfAvengers()
    {
        $result = $this->callApiService->getCommics();
        $this->assertIsArray($result);
        $this->assertEquals(200, $result['code']);
        $this->assertEquals('Ok', $result['status']);
        $this->assertEquals(50, count($result['data']['results']));
        $this->assertStringStartsWith('Avengers',$result['data']['results'][1]['title']);
    }
    //Pour tester getCharactersbyCommic de CallApiService
    public function testGetCharactersbyCommicId()
    {
        $result = $this->callApiService->getCharactersbyCommic(6951);
        $this->assertIsArray($result);
        $this->assertEquals(200, $result['code']);
        $this->assertEquals('Ok', $result['status']);
        $this->assertEquals(7, count($result['data']['results']));
    }


}
