<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class MarvelAPITesTest extends WebTestCase
{

    
    public function testHome(): void
    {
        $client = static::createClient();
        $client->request('GET', '/home');

        $this->assertResponseIsSuccessful();
    }
    public function testCharacterGetById() {
        
        $client = static::createClient();
        
        $client->request('GET', '/characters/1');
        $this->assertResponseIsSuccessful();        
    }

}
